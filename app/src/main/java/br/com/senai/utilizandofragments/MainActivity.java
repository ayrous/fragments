package br.com.senai.utilizandofragments;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button botaoTrocarFragmento;
    private Boolean status = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        botaoTrocarFragmento = findViewById(R.id.botaoTrocarFragment);

        botaoTrocarFragmento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                if(status == true){
                    LoginFragment fragmentoLogin = new LoginFragment();
                    fragmentTransaction.replace(R.id.conteiner_fragments, fragmentoLogin);
                    fragmentTransaction.commit();
                    botaoTrocarFragmento.setText("Contato");
                    status = false;
                }else{

                    ContatoFragment fragmentoContato = new ContatoFragment();
                    fragmentTransaction.replace(R.id.conteiner_fragments, fragmentoContato);
                    fragmentTransaction.commit();
                    botaoTrocarFragmento.setText("Login");
                    status = true;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_principal, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id == R.id.configId){

            Intent intent = new Intent(getApplicationContext(), ConfiguracoesActivity.class);
            startActivity(intent);

        } else if(id == R.id.historicoId){
            Intent intent = new Intent(getApplicationContext(), HistoricoActivity.class);
            startActivity(intent);


        }

        return super.onOptionsItemSelected(item);
    }
}