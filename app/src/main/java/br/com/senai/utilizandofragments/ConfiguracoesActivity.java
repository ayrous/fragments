package br.com.senai.utilizandofragments;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class ConfiguracoesActivity extends AppCompatActivity {

    private Button botaoSalvar;
    private RadioGroup radioGroup;
    private RadioButton botaoSelecionado;
    private static final String ARQUIVO_PREFERENCIA = "arqPreferencia";
    private ConstraintLayout telaConfiguracoes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracoes);

        botaoSalvar = findViewById(R.id.btnSalvar);
        telaConfiguracoes = findViewById(R.id.telaConfig);
        radioGroup = findViewById(R.id.radioGroup);

        botaoSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int idRadioSelecionado = radioGroup.getCheckedRadioButtonId();

                if(idRadioSelecionado > 0){

                    botaoSelecionado = findViewById(idRadioSelecionado);
                    SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    String corSelecionada = botaoSelecionado.getText().toString();
                    editor.putString("corSelecionada", corSelecionada);
                    editor.commit();
                    setBackground(corSelecionada);
                }
            }

        });

        SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        if(sharedPreferences.contains("corSelecionada")){
            String corRecuperada = sharedPreferences.getString("corSelecionada", "Padrão");
            setBackground(corRecuperada);
        }
    }

    private void setBackground(String corSelecionada){

        if(corSelecionada.equals("Azul")){
            telaConfiguracoes.setBackgroundColor(Color.parseColor("#2274f7"));

        }else if(corSelecionada.equals("Roxo")){
            telaConfiguracoes.setBackgroundColor(Color.parseColor("#6217b2"));

        }else if(corSelecionada.equals("Verde")){
            telaConfiguracoes.setBackgroundColor(Color.parseColor("#1ca540"));

        }else if(corSelecionada.equals("Padrão")){
            telaConfiguracoes.setBackgroundColor(Color.parseColor("#ffffff"));

        }

    }


}